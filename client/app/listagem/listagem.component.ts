import { Component } from '@angular/core';
import { Http } from "@angular/http";

@Component({
  moduleId: module.id,
  selector: "listagem",
  templateUrl: "./listagem.component.html"
})
export class ListagemComponent {
  //constructor(@Inject(Http) http){
  fotos: Array<Object> = [];
  // ou fotos: Object[] = [];
  constructor(http: Http) {
    http
      .get("v1/fotos")
      .map(res => res.json())
      .subscribe(
        fotos => {
          this.fotos = fotos;
          console.log(fotos);
        },
        erro => console.log(erro)
      );
  }
}
