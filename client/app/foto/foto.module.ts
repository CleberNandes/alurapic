import { NgModule } from '@angular/core';
import { FotoComponent } from "./foto.component";
import { FiltroTitulo } from "./foto.pipes";

@NgModule({
  // o que o molule tem
  declarations: [FotoComponent, FiltroTitulo],
  // o que outros modules podem importar
  exports: [FotoComponent, FiltroTitulo]
})
export class FotoModule {}