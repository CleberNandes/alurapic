import { Component } from '@angular/core';
import { FotoComponent } from '../foto/foto.component';

@Component({
  moduleId: module.id,
  selector: "cadastro",
  templateUrl: "./cadastro.component.html"
})
export class CadastroComponent {
	foto: FotoComponent = new FotoComponent();
	
	constructor(){
		this.foto.titulo = 'A';
		this.foto.url = 'B';
		this.foto.descricao = 'C';
	}
  cadastrar(event){
    // para evitar o carregamento da pagina usar o
    event.preventDefault();
    //aqui enviamos os dados via ajax 
    console.log(this.foto)

  }
}
