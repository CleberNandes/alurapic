import { NgModule } from '@angular/core';
import { BrowserModule } from "@angular/platform-browser";
import 'rxjs/add/operator/map';
import { rounting } from "./app.routes";
import { FormsModule } from "@angular/forms";

import { FotoModule } from "./foto/foto.module";
import { HttpModule } from "@angular/http";
import { PainelModule } from "./painel/painel.module";

import { AppComponent } from "./app.component";
import { CadastroComponent } from "./cadastro/cadastro.component";
import { ListagemComponent } from "./listagem/listagem.component";

@NgModule({
  // components que poderão ser usados pelo app atraves do module
  imports: [
    BrowserModule,
    FotoModule,
    HttpModule,
    PainelModule,
    rounting,
    FormsModule
  ],
  // serve para falar que os components fazem parte desse module
  declarations: [AppComponent, CadastroComponent, ListagemComponent],
  // até agora só o app raiz tem o attr bootstrap
  bootstrap: [AppComponent]
})
export class AppModule {}
