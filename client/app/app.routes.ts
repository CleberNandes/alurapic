import { Routes, RouterModule } from '@angular/router';

//Components que serão usados
import { ListagemComponent } from './listagem/listagem.component';
import { CadastroComponent } from './cadastro/cadastro.component';

//Rotas associadas ao component
const routes: Routes = [
    { path: '', component: ListagemComponent },
    { path: 'cadastro', component: CadastroComponent },
    // 404 
    { path: '**', component: ListagemComponent }
];

// ao invés de class usar const abaixo
export const rounting = RouterModule.forRoot(routes);


