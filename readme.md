pasta server
    npm start

tudo ficara dentro de uma pasta app dentro de client
criar pasta app
criar component principal dentro da pasta app

criar app.component.ts e colocar o conteudo:

    import {Component} from '@angular/core';
    @Component({
        selector: 'app',
        templateUrl: './app/app.component.html'
    })
    class AppComponent { }

criar o arquivo app.component.html dentro da pasta app e por o conteudo:

    <div class="jumbotron">
        <h1 class="text-center">AluraPic</h1>
    </div>
    <div class="container">
        <img src="img/leao.jpg" alt="leão" class="img-responsive center-block">
    </div>

instalar typescript que já está com dependencia no package.json com o comando 

    npm install

rodar na pasta client o comando

    npm start

através do transpile ele compila os arquivos TS em JS

sempre manter o server e o client com o npm start

colocar a tag 

    <app></app>

Criar o module do angular app.module.ts na pasta app
snippet do vscode para criar um NgModule
ng-module ele gera todo o codigo abaixo

    import { NgModule } from '@angular/core';
    import { CommonModule } from '@angular/common';
    @NgModule({
        declarations: [],
        imports: [ CommonModule ],
        exports: [],
        providers: [],
    })
    export class FeatureModule {}

Sempre que criar um Component tem que criar um NgModule

Na pasta app criar o main.ts que será invocado pelo System.config.json
quando o app for executado chamara ele
atalho para criar o conteudo do main.ts

    ng-bootstraping

quando rodar npm start na pasta cliente na verdade rodar

    npm run tsc:w 

compilador do typescript que fica listen a qualquer mudança

Aula 2

Criando o component foto
    ng-component 
    ng-module
foto.component.html

colocar a tag img dentro do foto.html

O component foto é diferente do component app, pois o app é a raiz.
assim no module só importamos, já no module foto exportamos
todos os outros componentes criados vamos sempre exporta-lo no seu module
para que outros componentes possam usa-lo

importar FotoModule no AppModule 

Para evitarmos colocar o caminho completo de um recurso,
pode por uma propriedade no @Component chamada
moduleId: module.id
em qualquer component que necessita de um templateUrl,
assim não é necessário o caminho completo do recurso
indicando a pasta do component, só o seu NgModule

Parametros do component

No component que aceitara parametros colocar o Input no import
    
    import { Component, Input } from '@angular/core';

dentro da classe mencionar os Input    
    @Input() url;
    @Input() titulo;

No html envolver com [] os attr que receberão parametros e mencionar quais
    
    <img [src]="url" [alt]="titulo" class="img-responsive center-block">

ou para formato do angular 1 usar a expressão:

    <img src="{{url}}" alt="{{titulo}}" class="img-responsive center-block">

AULA 3

Renderizando fotos do servidor

importar Http do angular no component app

    import { Http } from "@angular/http";

ele faz requisições via ajax para o backend
instanciar o http na classe app

para não invocarmos no construtor, vou usar 
injeção de dependencia
colocando ele como parametro do contrutor o angular se vira
para montar o http com seus devidos parametros
Mas para isso ser possível tem que usar o Inject co angular core

    import {Component, Inject} from '@angular/core';

e no constructor colocar o parametro http tipado

    constructor(@Inject(Http) http){ }

Mas para o angular conseguir resolver precisa
importar no modulo do app o http

    import { HttpModule } from "@angular/http";

adicionar nos imports do NgModule do app

    imports: [BrowserModule, FotoModule, HttpModule],

HttpModule ele tem o provider que precisa ser injetado no http

isso é uma forma de tipar em typescript

    constructor(@Inject(Http) http){ }

e essa é igual, fazem a mesma coisa

    constructor( http: Http ){ }

Aula 3.2

Para renderizar as fotos do server, criar um atributo do AppComponent

    fotos: Array<Object> = []; ou 
    fotos: Object[] = [];

atribuir a fotos o json que vem do server 
atraves do http acessamos a rota com metodo get
    let stream = http.get('v1/fotos');
e da um subscribe no retorno 
e dentro com aero function atribuimos o json da resposta a
propriedade fotos

    stream.subscribe( res => {
        this.fotos = res.json();
    });

ou sem instanciar uma stream

    http.get('v1/fotos')
    .subscribe( res => {
        this.fotos = res.json();
    });

usar map para simplificar a resposta do aero function
transformando em json, passando o obheto final para o subscribe
mas para o map funcionar precisa importar para todas as intancias
de um observable no app.module.ts

    import 'rxjs/add/operator/map';

adicionar uma segunda função no subscribe caso tenha erro

    .subscribe( fotos => {
        this.fotos = fotos;
    }, erro => console.log(erro));

para tornar o objeto foto iteravel alteramos seu html

    <foto *ngFor="let foto of fotos" [url]="foto.url" [titulo]="foto.titulo"></foto>  

usando o for do angular

    *ngFor="let foto of fotos"

e atribuindo o valor de cada item as propriedades do objeto

    [url]="foto.url" [titulo]="foto.titulo"

AULA 4

criando component painel lembrando, criar
module, component, html

importar no app.module o painel.module
no app.html involver a foto com o novo component painel
passar o *ngFor para o painel e incluir o atributo titulo

para indicar que um component pode receber outro component
incluir a tag ng-content no lugar

    <ng-content></ng-content>

Para uma melhor apresentação do painel incolver com uma div.row
e determnar um col-md-2 para cada um

Aula 4.2

criar component cadastro que será uma rota e o html

component que serão parte da route do app não tem module
porque o cadastro não será utilizado em outros lugares,
a não ser no app, por isso não precisa de module

importar o component no app.module

criar o component listagem sem module sendo rota do app
O conteudo html do app passa a ser do component listagem

nunca esquecer de atribuir o attr moduleId do component

Aula 4.3 route

crair o app.routes.ts
    ng-router criar a estrutura mais criando a class
Estrutura de um routes

    import { Routes, RouterModule } from '@angular/router';

Componentes que serão roteados

    import { ListagemComponent } from './listagem/listagem.component';
    import { CadastroComponent } from './cadastro/cadastro.component';

Rotas associadas ao componente

    const routes: Routes = [
        { path: '', component: ListagemComponent },
        { path: 'cadastro', component: CadastroComponent }
    ];

    export const rounting = RouterModule.forRoot(routes);

precisa substituir a class por 

    export const rounting = RouterModule.forRoot(routes);

para usar as routes importar no app.module o app.routes

    import { rounting } from "./app.routes";

e colocar nos imports attr do app.module

para fazer o sistema de hota funcionar tem que incluir no
index.html a linha

    <base href="/">

e por fim no arquivo app.component.html que está vazio por:

    <router-outlet></router-outlet>

incluir botão para ir a tela de cadastro

Quando usamos href em um link ele recarrega toda a aplicação
para evitar isso usar o 

    [routerLink]="['/cadastro']" ou 
    [routerLink]="'/cadastro'" ou 

como as rotas estão dentro de um array tem que passar a rota
requerida em forma de array tb ou
com aspas simples dentro das duplas

Aula 5

o simbulo abaixo é chamado de pipe
 |

Pode ser usado dentro de components.html para configurações
Ex para colocar o testo to UpperCase usar:

    [titulo]="foto.titulo | uppercase"

Search Angular 
para filtrar uma foto na lista tb usar pipe no html onde esta a lista

    *ngFor="let foto of fotos | filtroTitulo"

e uma identidicação no input com #nome, no caso

    #tituloProcurado

depois assimilar o o pipe recebendo o value do input

    *ngFor="let foto of fotos | filtroTitulo: tituloProcurado.value"

Para usar o pipe tb é preciso criar um decorator de pipe para o 
filtroTitulo dentro da pasta foto chamado foto.pipes.ts
atalho para preencher o pipe

    ng-pipe
cria a estrutura:

    import { Pipe, PipeTransform } from '@angular/core';
    @Pipe({name: 'filtroTitulo'})
    export class FiltroTitulo implements PipeTransform {
        transform(fotos, digitado) {
            console.log(fotos)
            console.log(digitado)
        }
    }

importar o pipe criado no foto.module

    import { FiltroTitulo } from "./foto.pipes";

no foto.module importar FiltroTitulo tanto no 
declarations[] quanto no exports

Aula 5.3
Filtrando de fato no foto.pipes transform()

    transform(fotos, digitado) {
        digitado = digitado.toLowerCase();
        return fotos.filter(foto => foto.titulo.toLowerCase()
            .includes(digitado));
    }

e por fim para funcionar precisa por um databind no input digitado
    (keyup)="0"
no lugar do 0(zero) poderia ser qualquer valor

Aula 5.4 Passar a usar mais a tipagem do typescript

    <input (keyup)="0" placeholder="filtrar pelo título da foto" 
        class="form-control" #tituloProcurado>
    <!-- var-textoProcurado ou #tituloProcurado são iguais -->

Aula 6.1 implementando o cadastro de fotos
no CadastroComponent criar propriedade foto

    export class CadastroComponent {
        foto: FotoComponent = new FotoComponent();
    }

criar no CadastroComponent o metodo 

    cadastrar(event){
        // para evitar o carregamento da pagina usar o
        event.preventDefault();
        console.log(this.foto)
    }

Aula 6.2 data bind unidirecional da view pára o model

no formulario do cadastrar colocar o submit do angular
apontando para o metodo cadatrar do CadastroComponent

    <form class="row" (submit)="cadastrar($event)">

Associar os inputs como propriedade da nova foto atraves do
attr [value]="foto.titulo"

two ways data-bind

model para a view

    <input [value]="foto.titulo">

view para model
(input)="foto.titulo = $event.target.value"

    <input [value]="foto.titulo" (input)="foto.titulo = $event.target.value">

implementar um constructor para o CadastroComponent

    constructor(){
        this.foto.titulo = 'A';
        this.foto.url = 'B';
        this.foto.descricao = 'C';
    }

#Aula 6.4 usando ngModel

vamos simplificar o 2-ways data-bind substituindo 

    <input [value]="foto.titulo" (input)="foto.titulo = $event.target.value">

por e [(ngModel)]="object.attr"

    <input [(ngModel)]="foto.titulo">

Mas para o input ngModel funcionar temos que importar um modulo do angular

    import { FormsModule } from "@angular/forms";

e nos imports do NgModels inseri-lo

para o ngModel funcionar tem que atribuir o name de cada input

    name="titulo"
    name="url"
    name="descricao"

#Aula 6.5
